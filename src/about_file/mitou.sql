/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 80022
Source Host           : localhost:3306
Source Database       : mitou

Target Server Type    : MYSQL
Target Server Version : 80022
File Encoding         : 65001

Date: 2021-04-17 13:14:11
*/

SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for base_menu
-- ----------------------------
DROP TABLE IF EXISTS `base_menu`;
CREATE TABLE `base_menu`
(
    `menu_id`          bigint       NOT NULL COMMENT '菜单ID',
    `parent_menu_id`   bigint                DEFAULT '0' COMMENT '菜单父ID',
    `menu_name`        varchar(100) NOT NULL DEFAULT '' COMMENT '菜单名称',
    `menu_code`        varchar(50)  NOT NULL DEFAULT '' COMMENT '菜单编码',
    `menu_url`         varchar(50)  NOT NULL DEFAULT '' COMMENT '菜单URL',
    `menu_type`        tinyint      NOT NULL COMMENT '菜单类型(0:目录;1:菜单;2:按钮)',
    `order_num`        smallint              DEFAULT '0' COMMENT '显示顺序',
    `create_user_name` varchar(20)           DEFAULT '' COMMENT '创建人',
    `create_user_id`   bigint                DEFAULT NULL COMMENT '创建人ID',
    `create_time`      datetime              DEFAULT NULL COMMENT '创建时间',
    PRIMARY KEY (`menu_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='系统管理--系统菜单';

-- ----------------------------
-- Table structure for base_role
-- ----------------------------
DROP TABLE IF EXISTS `base_role`;
CREATE TABLE `base_role`
(
    `role_id`          bigint       NOT NULL COMMENT '角色ID',
    `role_name`        varchar(100) NOT NULL                                  DEFAULT '' COMMENT '角色名称',
    `role_code`        varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '角色编码',
    `role_default`     tinyint      NOT NULL                                  DEFAULT '0' COMMENT '是否默认角色(0:非默认;1:默认)',
    `order_num`        smallint                                               DEFAULT '0' COMMENT '显示顺序',
    `sys_init`         tinyint      NOT NULL                                  DEFAULT '0' COMMENT '是否内置角色(0:非内置;1:内置)',
    `create_user_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建人',
    `create_user_id`   bigint                                                 DEFAULT NULL COMMENT '创建人ID',
    `create_time`      datetime                                               DEFAULT NULL COMMENT '创建时间',
    PRIMARY KEY (`role_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='系统管理--系统角色';

-- ----------------------------
-- Table structure for base_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `base_role_menu`;
CREATE TABLE `base_role_menu`
(
    `role_menu_id` bigint NOT NULL COMMENT '角色菜单ID',
    `role_id`      bigint NOT NULL COMMENT '角色ID',
    `menu_id`      bigint NOT NULL COMMENT '菜单ID',
    PRIMARY KEY (`role_menu_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='系统管理--角色权限关系';

-- ----------------------------
-- Table structure for base_user
-- ----------------------------
DROP TABLE IF EXISTS `base_user`;
CREATE TABLE `base_user`
(
    `user_id`        bigint                                                  NOT NULL COMMENT '用户ID',
    `user_name`      varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci           DEFAULT '' COMMENT '姓名',
    `login_name`     varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL DEFAULT '' COMMENT '登录名',
    `user_pwd`       varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '密码',
    `phone`          varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci           DEFAULT '' COMMENT '电话',
    `email`          varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci           DEFAULT '' COMMENT '邮箱',
    `gender`         varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci            DEFAULT '' COMMENT '性别',
    `user_logo`      varchar(100)                                                     DEFAULT '' COMMENT '用户头像',
    `birthday`       datetime                                                         DEFAULT NULL COMMENT '生日',
    `user_type`      varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci           DEFAULT '' COMMENT '用户类型',
    `create_user_id` bigint                                                           DEFAULT NULL COMMENT '创建人ID',
    `create_time`    datetime                                                         DEFAULT NULL COMMENT '创建时间',
    `del_flag`       tinyint                                                 NOT NULL DEFAULT '1' COMMENT '删除状态(0:未删除;1:已删除)',
    PRIMARY KEY (`user_id`),
    UNIQUE KEY `login_name` (`login_name`) USING BTREE COMMENT '登录名'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='系统管理--系统用户';

-- ----------------------------
-- Table structure for base_user_role
-- ----------------------------
DROP TABLE IF EXISTS `base_user_role`;
CREATE TABLE `base_user_role`
(
    `user_role_id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户角色关系ID',
    `user_id`      bigint NOT NULL COMMENT '用户ID',
    `role_id`      bigint NOT NULL COMMENT '角色ID',
    PRIMARY KEY (`user_role_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='系统管理--用户角色关系';

-- ----------------------------
-- Table structure for base_dict
-- ----------------------------
DROP TABLE IF EXISTS `base_dict`;
CREATE TABLE `base_dict`
(
    `dict_id`        bigint(20)                                      NOT NULL COMMENT '字典ID',
    `dict_name`      varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '字典名称',
    `parent_id`      bigint(20)                                      NULL DEFAULT 0 COMMENT '字典父id',
    `sort`           int(11)                                         NULL DEFAULT NULL COMMENT '顺序',
    `dict_type`      tinyint(4)                                      NULL DEFAULT NULL COMMENT '字典类型(1:字典组;2:字典值)',
    `remark`         varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '描述',
    `create_user_id` bigint(20)                                      NULL DEFAULT NULL COMMENT '创建人ID',
    `create_time`    datetime(0)                                     NULL DEFAULT NULL COMMENT '创建时间',
    PRIMARY KEY (`dict_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT = '系统管理--数据字典';