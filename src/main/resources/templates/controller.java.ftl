package ${package.Controller};

import com.baomidou.mybatisplus.core.metadata.IPage;
import ${parentPackage}.common.response.Result;
import ${parentPackage}.common.utils.dto.DeleteDto;
import ${parentModel}.service.${table.serviceName};
import ${parentModel}.entity.dto.${entity}Dto;
import ${parentModel}.entity.query.${entity}Query;
import ${parentModel}.entity.vo.${entity}Vo;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RequestMapping;
<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>
import java.util.List;
<#assign entityUncapFirst="${entity?uncap_first}"/>

/**
 * <p>
 * ${table.comment!}前端控制器
 * </p>
 *
 * @author ${author}
 * @date ${date}
 */
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequestMapping("<#if package.ModuleName?? && package.ModuleName != "">/${package.ModuleName?lower_case}</#if>/<#if controllerMappingHyphenStyle>${controllerMappingHyphen?lower_case}<#else>${entity?lower_case}</#if>")
<#if kotlin>
class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
<#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass} {
<#else>
@Api(value = "<#if package.ModuleName?? && package.ModuleName != "">/${package.ModuleName?lower_case}</#if>/<#if controllerMappingHyphenStyle>${controllerMappingHyphen?lower_case}<#else>${entity?lower_case}</#if>", tags = "${table.comment!}")
public class ${table.controllerName} {
</#if>
<#list table.fields as field>
    <#if field.keyFlag>
        <#assign keyPropertyType="${field.propertyType}"/>
        <#assign keyPropertyName="${field.propertyName}"/>
        <#assign keyPropertyNameCapital="${field.capitalName}"/>
        <#assign keyPropertyComment="${field.comment}"/>
    </#if>
</#list>

    @Resource
    private ${table.serviceName} ${entityUncapFirst}Service;

    @GetMapping("/{${keyPropertyName}}")
    @ApiOperation(value = "根据ID获取数据", httpMethod = "GET")
    @ApiImplicitParam(paramType = "path", name = "${keyPropertyName}", value = "ID标识", required = true)
    public Result${'<'}${entity}Vo> getById(@PathVariable ${keyPropertyType} ${keyPropertyName}) {
        return Result.success(${entityUncapFirst}Service.getById(${keyPropertyName}));
    }

    @GetMapping("/page")
    @ApiOperation(value = "分页查询", httpMethod = "GET")
    public Result${'<'}IPage${'<'}${entity}Vo>> page(@ModelAttribute ${entity}Query query) {
        return Result.success(${entityUncapFirst}Service.page(query));
    }

    @PostMapping
    @ApiOperation(value = "新增", httpMethod = "POST")
    public Result<Boolean> save(@RequestBody ${entity}Dto dto) {
        return Result.success(${entityUncapFirst}Service.insert(dto));
    }

    @PostMapping("/update")
    @ApiOperation(value = "根据ID修改数据", httpMethod = "POST")
    public Result<Boolean> update(@RequestBody ${entity}Dto dto) {
        return Result.success(${entityUncapFirst}Service.updateByIdSelective(dto));
    }

    @DeleteMapping("/deleteByIds")
    @ApiOperation(value = "根据ID批量删除", httpMethod = "POST")
    public Result<Boolean> deleteByIds(@RequestBody DeleteDto dto) {
        return Result.success(${entityUncapFirst}Service.removeByIds(dto.getIdList()));
    }

}
</#if>
