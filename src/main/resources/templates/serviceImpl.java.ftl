package ${parentModel}.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.core.metadata.IPage;
import ${superServiceImplClassPackage};
import ${parentModel}.entity.${entity}Entity;
import ${parentModel}.entity.dto.${entity}Dto;
import ${parentModel}.entity.query.${entity}Query;
import ${parentModel}.entity.vo.${entity}Vo;
import ${parentModel}.mapper.${table.mapperName};
import ${parentModel}.mapstruct.${entity}Struct;
import ${parentModel}.service.${table.serviceName};
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

<#list table.fields as field>
    <#if field.keyFlag>
        <#assign keyPropertyType="${field.propertyType}"/>
        <#assign keyPropertyName="${field.propertyName}"/>
        <#assign keyPropertyNameCapital="${field.capitalName}"/>
        <#assign keyPropertyComment="${field.comment}"/>
    </#if>
</#list>
<#assign entityUncapFirst="${entity?uncap_first}"/>
/**
 * <p>
 * ${table.comment!} 服务实现类
 * </p>
 *
 * @author ${author}
 * @date ${date}
 */
@Service
<#if kotlin>
open class ${table.serviceImplName} : ${superServiceImplClass}<${table.mapperName}, ${entity}>(), ${table.serviceName} {

}
<#else>
public class ${table.serviceImplName} extends ${superServiceImplClass}<${table.mapperName}, ${entity}Entity> implements ${table.serviceName} {

    @Resource
    private ${entity}Mapper ${entityUncapFirst}Mapper;
    @Resource
    private ${entity}Struct ${entityUncapFirst}Struct;


    @Override
    public ${entity}Vo getById(${keyPropertyType} ${keyPropertyName}){
        ${entity}Entity ${entityUncapFirst} = super.getById(${keyPropertyName});
        return ${entityUncapFirst}Struct.convert(${entityUncapFirst});
    }

    @Override
    public IPage<${entity}Vo> page(${entity}Query query) {
        LambdaQueryWrapper<${entity}Entity> lqw = Wrappers.lambdaQuery();
        //进行条件构造

        IPage page = super.page(query.page(), lqw);
        List<${entity}Vo> ${entityUncapFirst}Vos = ${entityUncapFirst}Struct.converts(page.getRecords());
        page.setRecords(${entityUncapFirst}Vos);
        return page;
    }

    @Override
    public boolean insert(${entity}Dto dto){
        ${entity}Entity ${entityUncapFirst} = ${entityUncapFirst}Struct.convertEntity(dto);
        return super.save(${entityUncapFirst});
    }

    @Override
    public boolean updateByIdSelective(${entity}Dto dto){
        ${entity}Entity ${entityUncapFirst} = ${entityUncapFirst}Struct.convertEntity(dto);
        return super.updateById(${entityUncapFirst});
    }
}
</#if>
