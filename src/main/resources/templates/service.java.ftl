package ${parentModel}.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import ${superServiceClassPackage};
import ${parentModel}.entity.${entity}Entity;
import ${parentModel}.entity.dto.${entity}Dto;
import ${parentModel}.entity.query.${entity}Query;
import ${parentModel}.entity.vo.${entity}Vo;

<#list table.fields as field>
 <#if field.keyFlag>
  <#assign keyPropertyType="${field.propertyType}"/>
  <#assign keyPropertyName="${field.propertyName}"/>
  <#assign keyPropertyNameCapital="${field.capitalName}"/>
  <#assign keyPropertyComment="${field.comment}"/>
 </#if>
</#list>
<#assign entityUncapFirst="${entity?uncap_first}"/>
/**
 * <p>
 * ${table.comment!} 服务接口类
 * </p>
 *
 * @author ${author}
 * @date ${date}
 */
<#if kotlin>
interface ${table.serviceName} : ${superServiceClass}<${entity}Entity>
<#else>
public interface ${table.serviceName} extends ${superServiceClass}<${entity}Entity> {


    /**
     * 根据ID获取数据
     *
     * @param ${keyPropertyName} id
     * @return Vo
     */
    ${entity}Vo getById(${keyPropertyType} ${keyPropertyName});

    /**
     * 分页查询
     *
     * @param query 条件参数
     * @return 分页对象
     */
    IPage${'<'}${entity}Vo> page(${entity}Query query);

    /**
     * 新增
     *
     * @param dto 请求参数
     * @return 执行结果
     */
    boolean insert(${entity}Dto dto);

    /**
     * 根据ID修改数据
     *
     * @param dto 请求参数
     * @return 执行结果
     */
    boolean updateByIdSelective(${entity}Dto dto);

}
</#if>
