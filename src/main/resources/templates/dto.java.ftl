package ${parentModel}.entity.dto;

<#list table.importPackages as pkg>
    <#if pkg=="java.util.Date">
import ${pkg};
    </#if>
</#list>
<#list table.fields as field>
    <#if field.propertyType == "BigDecimal">
import java.math.BigDecimal;
    <#break>
    </#if>
</#list>

<#if swagger>
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
</#if>
<#if entityLombokModel>
import lombok.Data;
    <#if chainModel>
import lombok.experimental.Accessors;
    </#if>
</#if>

/**
 * <p>
 * ${table.comment!}--请求参数
 * </p>
 *
 * @author ${author}
 * @date ${date}
 */
<#if entityLombokModel>
@Data
    <#if chainModel>
@Accessors(chain = true)
    </#if>
</#if>
<#if swagger>
@ApiModel(value = "${entity}请求参数", description = "${table.comment!}--请求参数")
</#if>
public class ${entity}Dto {
<#-- ----------  BEGIN 字段循环遍历  ---------->
<#list table.fields as field>
    <#if field.keyFlag>
        <#assign keyPropertyName="${field.propertyName}"/>
    </#if>
    <#if field.propertyName != "createTime"
        && field.propertyName != "createBy"
        && field.propertyName != "createUsername"
        && field.propertyName != "updateBy"
        && field.propertyName != "updateTime"
        && field.propertyName != "updateUsername">

    @ApiModelProperty("${field.comment}")
    private ${field.propertyType} ${field.propertyName};
    </#if>
</#list>
}
