package ${parentModel}.entity.query;

<#list table.importPackages as pkg>
    <#if pkg=="java.util.Date">
import ${pkg};
    </#if>
</#list>
<#list table.fields as field>
    <#if field.propertyType == "BigDecimal">
import java.math.BigDecimal;
    <#break>
    </#if>
</#list>

<#if swagger>
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
</#if>
<#if entityLombokModel>
import lombok.Data;
import lombok.EqualsAndHashCode;
    <#if chainModel>
import lombok.experimental.Accessors;
    </#if>
</#if>
import ${parentPackage}.common.utils.query.PageQuery;

/**
 * <p>
 * ${table.comment!}--分页请求参数
 * </p>
 *
 * @author ${author}
 * @date ${date}
 */
<#if entityLombokModel>
@Data
@EqualsAndHashCode(callSuper = true)
    <#if chainModel>
@Accessors(chain = true)
    </#if>
</#if>
<#if swagger>
@ApiModel(value = "${entity}分页请求", description = "${table.comment!}--分页请求参数")
</#if>
public class ${entity}Query extends PageQuery {
<#-- ----------  BEGIN 字段循环遍历  ---------->
<#list table.fields as field>
    <#if field.keyFlag>
        <#assign keyPropertyName="${field.propertyName}"/>
    </#if>
    <#if field.propertyName != "createTime"
        && field.propertyName != "createBy"
        && field.propertyName != "updateBy"
        && field.propertyName != "updateTime">

    @ApiModelProperty("${field.comment}")
    private ${field.propertyType} ${field.propertyName};
    </#if>
</#list>
}
