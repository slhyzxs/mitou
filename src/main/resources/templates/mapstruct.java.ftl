package ${parentModel}.mapstruct;

import ${parentModel}.entity.vo.${entity}Vo;
import ${parentModel}.entity.${entity}Entity;
import ${parentModel}.entity.dto.${entity}Dto;
import org.mapstruct.Mapper;

import java.util.List;
<#assign entityUncapFirst="${entity?uncap_first}"/>

/**
 * <p>
 * ${table.comment!} 对象转换
 * </p>
 *
 * @author ${author}
 * @date ${date}
 */
@Mapper(componentModel = "spring")
public interface ${entity}Struct {

    /**
     * 实体转换为Vo
     *
     * @param ${entityUncapFirst}Entity 实体对象
     * @return Vo
     */
    ${entity}Vo convert(${entity}Entity ${entityUncapFirst}Entity);

    /**
     * 实体对象集合转换为Vo集合
     *
     * @param ${entityUncapFirst}EntityList 实体对象集合
     * @return Vo
     */
    List${'<'}${entity}Vo> converts(List${'<'}${entity}Entity> ${entityUncapFirst}EntityList);

    /**
     * Dto转换为实体
     *
     * @param ${entityUncapFirst}Dto dto
     * @return 实体对象
     */
    ${entity}Entity convertEntity(${entity}Dto ${entityUncapFirst}Dto);

}
