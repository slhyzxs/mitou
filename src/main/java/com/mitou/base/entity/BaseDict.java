package com.mitou.base.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;


/**
 * <p>
 * 系统管理--数据字典
 * </p>
 *
 * @author rice
 * @since 2022-01-04
 */
@Getter
@Setter
@TableName("base_dict")
public class BaseDict implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    @TableId(value = "dict_id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "ID标识")
    private Long dictId;

    @TableField("dict_name")
    @ApiModelProperty(value = "字典名称")
    private String dictName;

    @JsonSerialize(using = ToStringSerializer.class)
    @TableField("parent_id")
    @ApiModelProperty(value = "字典父id")
    private Long parentId;

    @TableField("sort")
    @ApiModelProperty(value = "顺序")
    @OrderBy(asc = true)
    private Integer sort;

    @TableField("dict_type")
    @ApiModelProperty(value = "字典类型(1:字典组;2:字典值)")
    private Integer dictType;

    @JsonIgnore
    @TableField("remark")
    @ApiModelProperty(value = "描述")
    private String remark;

    @JsonIgnore
    @TableField(value = "create_user_id", fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人ID")
    private Long createUserId;

    @JsonIgnore
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

}
