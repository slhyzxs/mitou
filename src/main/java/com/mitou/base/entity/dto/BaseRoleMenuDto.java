package com.mitou.base.entity.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


/**
 * <p>
 * 角色菜单关系
 * </p>
 *
 * @author rice
 * @since 2021-03-24
 */
@Getter
@Setter
@ApiModel(description = "角色菜单关系")
public class BaseRoleMenuDto {

    @ApiModelProperty(value = "角色ID", required = true)
    private Long roleId;

    @ApiModelProperty(value = "菜单ID集合", required = true)
    private List<Long> menuIdList;

}
