package com.mitou.base.entity.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


/**
 * <p>
 * 用户角色关系
 * </p>
 *
 * @author rice
 * @since 2021-03-24
 */
@Getter
@Setter
@ApiModel(description = "用户角色关系")
public class BaseUserRoleDto {

    @ApiModelProperty(value = "用户ID", required = true)
    private Long userId;

    @ApiModelProperty(value = "角色ID集合", required = true)
    private List<Long> roleIdList;

}
