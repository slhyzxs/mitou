package com.mitou.base.entity.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 更新密码的请求参数
 * </p>
 *
 * @author rice
 * @since 2021-03-25
 */
@Getter
@Setter
@ApiModel(description = "更新密码")
public class BaseUserUpdatePwdDto {

    @ApiModelProperty(value = "用户ID")
    private Long userId;

    @ApiModelProperty(value = "原密码")
    private String oldUserPwd;

    @ApiModelProperty(value = "密码")
    private String userPwd;

}
