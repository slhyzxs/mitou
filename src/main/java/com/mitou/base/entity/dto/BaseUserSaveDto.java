package com.mitou.base.entity.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * <p>
 * 新增--系统用户 的请求参数
 * </p>
 *
 * @author rice
 * @since 2021-03-25
 */
@Getter
@Setter
@ApiModel(description = "新增用户")
public class BaseUserSaveDto {

    @ApiModelProperty(value = "登录名")
    private String loginName;

    @ApiModelProperty(value = "姓名")
    private String userName;

    @ApiModelProperty(value = "密码")
    private String userPwd;

    @ApiModelProperty(value = "电话")
    private String phone;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "性别")
    private String gender;

    @ApiModelProperty(value = "用户头像")
    private String userLogo;

    @ApiModelProperty(value = "生日")
    private Date birthday;

    @ApiModelProperty(value = "组织id")
    private Long orgId;

    @ApiModelProperty(value = "组织")
    private String orgName;

    @ApiModelProperty(value = "职位")
    private String position;

    @ApiModelProperty(value = "部门")
    private String dept;

}
