package com.mitou.base.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;


/**
 * <p>
 * 系统用户
 * </p>
 *
 * @author rice
 * @since 2021-03-25
 */
@Getter
@Setter
@TableName("base_user")
public class BaseUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    @TableId(value = "user_id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    @TableField("login_name")
    @ApiModelProperty(value = "登录名")
    private String loginName;

    @TableField("user_name")
    @ApiModelProperty(value = "姓名")
    private String userName;

    @TableField("user_pwd")
    @ApiModelProperty(value = "密码")
    private String userPwd;

    @TableField("phone")
    @ApiModelProperty(value = "电话")
    private String phone;

    @TableField("email")
    @ApiModelProperty(value = "邮箱")
    private String email;

    @TableField("gender")
    @ApiModelProperty(value = "性别")
    private String gender;

    @TableField("user_logo")
    @ApiModelProperty(value = "用户头像")
    private String userLogo;

    @TableField("birthday")
    @ApiModelProperty(value = "生日")
    private Date birthday;

    @TableField("user_type")
    @ApiModelProperty(value = "用户类型")
    private String userType;

    @OrderBy
    @TableField("create_time")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @TableField("del_flag")
    @ApiModelProperty(value = "删除状态(0:未删除;1:已删除)")
    private Integer delFlag;

    @TableField(exist = false)
    @ApiModelProperty(value = "权限集", hidden = true)
    private Set<String> permissionSet;

}
