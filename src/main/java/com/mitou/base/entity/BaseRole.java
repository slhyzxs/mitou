package com.mitou.base.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;


/**
 * <p>
 * 系统角色
 * </p>
 *
 * @author rice
 * @since 2021-03-25
 */
@Getter
@Setter
@TableName("base_role")
public class BaseRole implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    @TableId(value = "role_id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "角色ID")
    private Long roleId;

    @TableField("role_name")
    @ApiModelProperty(value = "角色名称")
    private String roleName;

    @TableField("role_default")
    @ApiModelProperty(value = "是否默认角色(0:非默认;1:默认)")
    private Integer roleDefault;

    @OrderBy(asc = true, sort = 1)
    @TableField("order_num")
    @ApiModelProperty(value = "显示顺序")
    private Integer orderNum;

    @TableField("sys_init")
    @ApiModelProperty(value = "是否内置角色(0:非内置;1:内置)")
    private Integer sysInit;

    @TableField("create_user_name")
    @ApiModelProperty(value = "创建人")
    private String createUserName;

    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "create_user_id", fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人ID")
    private Long createUserId;

    @OrderBy(sort = 2)
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

}
