package com.mitou.base.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;


/**
 * <p>
 * 系统菜单
 * </p>
 *
 * @author rice
 * @since 2021-03-25
 */
@Getter
@Setter
@TableName("base_menu")
public class BaseMenu implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    @TableId(value = "menu_id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "菜单ID")
    private Long menuId;

    @JsonSerialize(using = ToStringSerializer.class)
    @TableField("parent_menu_id")
    @ApiModelProperty(value = "菜单父ID")
    private Long parentMenuId;

    @TableField("menu_name")
    @ApiModelProperty(value = "菜单名称")
    private String menuName;

    @TableField("menu_code")
    @ApiModelProperty(value = "菜单编码")
    private String menuCode;

    @TableField("menu_url")
    @ApiModelProperty(value = "菜单URL")
    private String menuUrl;

    @TableField("menu_type")
    @ApiModelProperty(value = "菜单类型(0:目录;1:菜单;2:按钮)")
    private Integer menuType;

    @OrderBy(asc = true, sort = 1)
    @TableField("order_num")
    @ApiModelProperty(value = "显示顺序")
    private Integer orderNum;

    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "create_user_id", fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人ID")
    private Long createUserId;

    @OrderBy(sort = 2)
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

}
