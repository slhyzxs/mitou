package com.mitou.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;


/**
 * <p>
 * 角色权限关系
 * </p>
 *
 * @author rice
 * @since 2021-03-25
 */
@Getter
@Setter
@TableName("base_role_menu")
public class BaseRoleMenu implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    @TableId(value = "role_menu_id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "角色菜单ID")
    private Long roleMenuId;

    @JsonSerialize(using = ToStringSerializer.class)
    @TableField("role_id")
    @ApiModelProperty(value = "角色ID")
    private Long roleId;

    @JsonSerialize(using = ToStringSerializer.class)
    @TableField("menu_id")
    @ApiModelProperty(value = "菜单ID")
    private Long menuId;

}
