package com.mitou.base.entity.query;

import com.mitou.common.utils.query.PageQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 分页查询--系统管理--数据字典的分页条件Vo
 * </p>
 *
 * @author rice
 * @since 2022-01-04
 */
@Getter
@Setter
@ApiModel(description = "系统管理--数据字典分页条件Vo")
public class BaseDictQuery extends PageQuery {

    @ApiModelProperty(value = "字典父id")
    private Long parentId;

}
