package com.mitou.base.entity.query;

import com.mitou.common.utils.query.PageQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 分页查询--系统角色 的请求参数 vo
 * </p>
 *
 * @author rice
 * @since 2021-03-25
 */
@Getter
@Setter
@ApiModel(description = "系统角色分页条件Vo")
public class BaseRoleQuery extends PageQuery {

    @ApiModelProperty(value = "角色名称")
    private String roleName;

    @ApiModelProperty(value = "是否内置角色(0:非内置 1:内置)")
    private Integer sysInit;

    @ApiModelProperty(value = "删除状态(0:已删除 1:正常)，不传默认查询未删除的")
    private Integer delFlag;

}
