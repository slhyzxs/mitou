package com.mitou.base.entity.query;

import com.mitou.common.utils.query.PageQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 分页查询--系统用户 的请求参数 vo
 * </p>
 *
 * @author rice
 * @since 2021-03-25
 */
@Getter
@Setter
@ApiModel(description = "系统用户分页条件Vo")
public class BaseUserQuery extends PageQuery {

    @ApiModelProperty(value = "姓名")
    private String userName;
    
    @ApiModelProperty(value = "电话")
    private String phone;

    @ApiModelProperty(value = "删除状态(0:未删除;1:已删除)")
    private Integer delFlag;

}
