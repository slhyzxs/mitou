package com.mitou.base.entity.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户下权限清单条件Vo
 * </p>
 *
 * @author rice
 * @since 2021-03-25
 */
@Getter
@Setter
@Accessors(chain = true)
@ApiModel(description = "用户下权限清单条件Vo")
public class BaseMenuHasQuery {

    @ApiModelProperty(value = "菜单类型(0:目录;1:菜单;2:按钮)", example = "1")
    private Integer menuType;

    @ApiModelProperty(value = "菜单父ID", example = "0")
    private Long parentMenuId;

    @ApiModelProperty(value = "用户ID", notes = "不传，默认查询当前登录用户", hidden = true)
    private Long userId;
}
