package com.mitou.base.entity.query;

import com.mitou.common.utils.query.PageQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 分页查询--菜单 的请求参数 vo
 * </p>
 *
 * @author rice
 * @since 2021-03-25
 */
@Getter
@Setter
@ApiModel(description = "菜单分页条件Vo")
public class BaseMenuQuery extends PageQuery {

    @ApiModelProperty(value = "菜单名称")
    private String menuName;

    @ApiModelProperty(value = "菜单类型(0:目录;1:菜单;2:按钮)")
    private Integer menuType;

}
