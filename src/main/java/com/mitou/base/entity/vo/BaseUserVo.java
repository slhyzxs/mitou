package com.mitou.base.entity.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.mitou.base.entity.BaseRole;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;


/**
 * <p>
 * 用户信息返回
 * </p>
 *
 * @author rice
 * @since 2021-03-25
 */
@Getter
@Setter
@ApiModel(description = "用户信息返回Vo")
public class BaseUserVo {

    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "用户ID", position = 0)
    private Long userId;

    @ApiModelProperty(value = "登录名", position = 1)
    private String loginName;

    @ApiModelProperty(value = "姓名", position = 2)
    private String userName;

    @ApiModelProperty(value = "密码", position = 3)
    private String userPwd;

    @ApiModelProperty(value = "电话", position = 4)
    private String phone;

    @ApiModelProperty(value = "邮箱", position = 5)
    private String email;

    @ApiModelProperty(value = "性别", position = 6)
    private String gender;

    @ApiModelProperty(value = "用户头像", position = 7)
    private String userLogo;

    @ApiModelProperty(value = "生日", position = 8)
    private Date birthday;

    @ApiModelProperty(value = "创建时间", position = 13)
    private Date createTime;

    @ApiModelProperty(value = "角色集", position = 14)
    private List<BaseRole> roleList;

}
