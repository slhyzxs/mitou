package com.mitou.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;


/**
 * <p>
 * 用户角色关系
 * </p>
 *
 * @author rice
 * @since 2021-03-25
 */
@Getter
@Setter
@TableName("base_user_role")
public class BaseUserRole implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    @TableId(value = "user_role_id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "用户角色关系ID")
    private Long userRoleId;

    @JsonSerialize(using = ToStringSerializer.class)
    @TableField("user_id")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    @JsonSerialize(using = ToStringSerializer.class)
    @TableField("role_id")
    @ApiModelProperty(value = "角色ID")
    private Long roleId;

}
