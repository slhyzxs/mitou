package com.mitou.base.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mitou.base.entity.BaseMenu;
import com.mitou.base.entity.dto.BaseRoleMenuDto;
import com.mitou.base.entity.query.BaseMenuHasQuery;
import com.mitou.base.entity.query.BaseMenuQuery;
import com.mitou.base.entity.vo.BaseMenuTreeVo;
import com.mitou.base.entity.vo.BaseMenuVo;
import com.mitou.base.service.IBaseMenuService;
import com.mitou.common.annotation.RoleAuth;
import com.mitou.common.response.Result;
import com.mitou.common.utils.dto.DeleteDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 菜单api
 * <p/>
 *
 * @author rice
 * @since 2021-03-25
 */
@RestController
@RequestMapping("/menu")
@Api(value = "/menu", tags = "系统管理--系统菜单")
public class BaseMenuController {

    @Resource
    private IBaseMenuService baseMenuService;

    @GetMapping("/{menuId}")
    @ApiOperation(value = "查询", httpMethod = "GET")
    @ApiImplicitParam(paramType = "path", name = "menuId", value = "菜单ID", required = true)
    public Result<BaseMenu> getById(@PathVariable Long menuId) {
        return Result.success(baseMenuService.getById(menuId));
    }

    @GetMapping
    @ApiOperation(value = "分页查询", httpMethod = "GET")
    public Result<IPage<BaseMenu>> page(@ModelAttribute BaseMenuQuery query) {
        return Result.success(baseMenuService.page(query));
    }

    @GetMapping("/has")
    @ApiOperation(value = "查询当前用户拥有权限的菜单列表", httpMethod = "GET", notes = "查询顶级菜单,parentMenuId传0")
    public Result<List<BaseMenuVo>> selectHas(@ModelAttribute BaseMenuHasQuery baseMenuHasQuery) {
        return Result.success(baseMenuService.selectHas(baseMenuHasQuery));
    }

    @GetMapping("/tree")
    @RoleAuth("menu::tree")
    @ApiOperation(value = "菜单树接口", httpMethod = "GET")
    public Result<List<BaseMenuTreeVo>> getTree() {
        return Result.success(baseMenuService.getTree());
    }

    @PostMapping
    @ApiOperation(value = "新增", httpMethod = "POST")
    public Result<Boolean> insert(@RequestBody BaseMenu baseMenu) {
        return Result.success(baseMenuService.save(baseMenu));
    }

    @PostMapping("/rel")
    @ApiOperation(value = "设置菜单", httpMethod = "POST", notes = "为角色设置菜单，会覆盖之前的菜单")
    public Result<Boolean> rel(@RequestBody BaseRoleMenuDto baseRoleMenuDto) {
        return Result.success(baseMenuService.rel(baseRoleMenuDto));
    }

    @PostMapping("/update")
    @ApiOperation(value = "更新", httpMethod = "POST")
    public Result<Boolean> update(@RequestBody BaseMenu baseMenu) {
        return Result.success(baseMenuService.updateById(baseMenu));
    }

    @PostMapping("/deleteByIds")
    @ApiOperation(value = "批量删除", httpMethod = "POST")
    public Result<Boolean> deleteByIds(@RequestBody DeleteDto dto) {
        return Result.success(baseMenuService.deleteByIds(dto.getIdList()));
    }
}
