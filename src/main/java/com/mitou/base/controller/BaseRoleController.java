package com.mitou.base.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mitou.base.entity.BaseRole;
import com.mitou.base.entity.dto.BaseUserRoleDto;
import com.mitou.base.entity.query.BaseRoleQuery;
import com.mitou.base.service.IBaseRoleService;
import com.mitou.common.response.Result;
import com.mitou.common.utils.dto.DeleteDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 菜单api
 * <p/>
 *
 * @author rice
 * @since 2021-03-25
 */
@RestController
@RequestMapping("/role")
@Api(value = "/role", tags = "系统管理--系统角色")
public class BaseRoleController {

    @Resource
    private IBaseRoleService baseRoleService;

    @GetMapping("/{roleId}")
    @ApiOperation(value = "查询", httpMethod = "GET")
    @ApiImplicitParam(paramType = "path", name = "roleId", value = "角色ID", required = true)
    public Result<BaseRole> getById(@PathVariable Long roleId) {
        return Result.success(baseRoleService.getById(roleId));
    }

    @GetMapping
    @ApiOperation(value = "分页查询", httpMethod = "GET")
    public Result<IPage<BaseRole>> page(@ModelAttribute BaseRoleQuery query) {
        return Result.success(baseRoleService.page(query));
    }

    @GetMapping("/user")
    @ApiOperation(value = "查询单个用户的角色列表", httpMethod = "GET")
    public Result<List<BaseRole>> getByUserId(@ApiParam(value = "用户ID，不传则查询当前登录人的") @RequestParam(value = "userId", required = false) Long userId) {
        return Result.success(baseRoleService.getByUserId(userId));
    }

    @PostMapping
    @ApiOperation(value = "新增", httpMethod = "POST")
    public Result<Boolean> insert(@RequestBody BaseRole baseRole) {
        return Result.success(baseRoleService.save(baseRole));
    }

    @PostMapping("/rel")
    @ApiOperation(value = "设置角色", httpMethod = "POST", notes = "为用户设置角色，会覆盖之前的角色")
    public Result<Boolean> rel(@RequestBody BaseUserRoleDto baseUserRoleDto) {
        return Result.success(baseRoleService.rel(baseUserRoleDto));
    }

    @PostMapping("/update")
    @ApiOperation(value = "更新", httpMethod = "POST")
    public Result<Boolean> update(@RequestBody BaseRole baseRole) {
        return Result.success(baseRoleService.updateById(baseRole));
    }

    @PostMapping("/deleteByIds")
    @ApiOperation(value = "批量删除", httpMethod = "POST")
    public Result<Boolean> deleteByIds(@RequestBody DeleteDto dto) {
        return Result.success(baseRoleService.deleteByIds(dto.getIdList()));
    }
}
