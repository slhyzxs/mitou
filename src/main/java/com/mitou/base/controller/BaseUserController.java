package com.mitou.base.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mitou.base.entity.BaseUser;
import com.mitou.base.entity.dto.BaseUserLoginDto;
import com.mitou.base.entity.dto.BaseUserSaveDto;
import com.mitou.base.entity.dto.BaseUserUpdateDto;
import com.mitou.base.entity.dto.BaseUserUpdatePwdDto;
import com.mitou.base.entity.query.BaseUserQuery;
import com.mitou.base.entity.vo.BaseUserLoginVo;
import com.mitou.base.entity.vo.BaseUserVo;
import com.mitou.base.service.IBaseUserService;
import com.mitou.common.annotation.OpenAuth;
import com.mitou.common.annotation.RoleAuth;
import com.mitou.common.response.Result;
import com.mitou.common.utils.dto.DeleteDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 菜单api
 * <p/>
 *
 * @author rice
 * @since 2021-03-25
 */
@RestController
@RequestMapping("/user")
@Api(value = "/user", tags = "系统管理--系统用户")
public class BaseUserController {

    @Resource
    private IBaseUserService baseUserService;

    @OpenAuth
    @PostMapping("/login")
    @ApiOperation(value = "登录", httpMethod = "POST", notes = "返回token")
    public Result<BaseUserLoginVo> login(@RequestBody BaseUserLoginDto baseUserLoginDto) {
        return baseUserService.login(baseUserLoginDto);
    }

    @OpenAuth
    @PostMapping("/logout")
    @ApiOperation(value = "退出登录", httpMethod = "POST", notes = "退出登录")
    public Result<Boolean> logout() {
        return Result.success(baseUserService.logout());
    }

    @GetMapping("/{userId}")
    @ApiOperation(value = "查询", httpMethod = "GET")
    @ApiImplicitParam(paramType = "path", name = "userId", value = "用户ID", required = true)
    public Result<BaseUserVo> getById(@PathVariable Long userId) {
        return Result.success(baseUserService.getById(userId));
    }

    @GetMapping
    @ApiOperation(value = "分页查询", httpMethod = "GET")
    //配置访问此方法的权限码。取的是菜单code
    @RoleAuth("user::page")
    public Result<IPage<BaseUser>> page(@ModelAttribute BaseUserQuery query) {
        return Result.success(baseUserService.page(query));
    }

    @OpenAuth
    @PostMapping("/register")
    @ApiOperation(value = "注册", httpMethod = "POST")
    public Result<Boolean> register(@RequestBody BaseUserSaveDto saveDto) {
        return Result.success(baseUserService.register(saveDto));
    }

    @GetMapping("/checkLegal")
    @ApiOperation(value = "检查登录名是否合法", httpMethod = "GET", notes = "true：合法；false：不合法")
    public Result<Boolean> checkLoginNameLegal(
            @ApiParam(value = "登录名", name = "loginName") @RequestParam(value = "loginName") String loginName) {
        return Result.success(baseUserService.checkLoginNameLegal(loginName));
    }

    @PostMapping("/{userId}")
    @ApiOperation(value = "更新", httpMethod = "POST")
    public Result<Boolean> update(@PathVariable("userId") Long userId, @RequestBody BaseUserUpdateDto updateDto) {
        BaseUser baseUser = new BaseUser();
        BeanUtils.copyProperties(updateDto, baseUser);
        baseUser.setUserId(userId);
        return Result.success(baseUserService.updateById(baseUser));
    }

    @PostMapping("/pwd")
    @ApiOperation(value = "更新密码", httpMethod = "POST")
    public Result<Boolean> updatePwd(@RequestBody BaseUserUpdatePwdDto updatePwdDto) {
        return baseUserService.updatePwd(updatePwdDto);
    }

    @PostMapping("/deleteByIds")
    @ApiOperation(value = "批量删除", httpMethod = "POST")
    public Result<Boolean> deleteByIds(@RequestBody DeleteDto dto) {
        return Result.success(baseUserService.deleteByIds(dto.getIdList()));
    }
}
