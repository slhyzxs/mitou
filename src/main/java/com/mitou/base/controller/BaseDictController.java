package com.mitou.base.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mitou.base.entity.BaseDict;
import com.mitou.base.entity.query.BaseDictQuery;
import com.mitou.base.service.IBaseDictService;
import com.mitou.common.response.Result;
import com.mitou.common.utils.dto.DeleteDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 系统字典api
 * <p/>
 *
 * @author rice
 * @since 2022-01-04
 */
@RestController
@RequestMapping("/baseDict")
@Api(value = "/baseDict", tags = "系统管理--系统字典")
public class BaseDictController {

    @Resource
    private IBaseDictService baseDictService;


    @GetMapping("/page")
    @ApiOperation(value = "分页查询", httpMethod = "GET")
    public Result<IPage<BaseDict>> page(@ModelAttribute BaseDictQuery query) {
        return Result.success(baseDictService.page(query));
    }

    @PostMapping("/insert")
    @ApiOperation(value = "新增", httpMethod = "POST")
    public Result<Boolean> insert(@RequestBody BaseDict baseDict) {
        return Result.success(baseDictService.insert(baseDict));
    }

    @PostMapping("/update")
    @ApiOperation(value = "更新", httpMethod = "POST")
    public Result<Boolean> update(@RequestBody BaseDict baseDict) {
        return Result.success(baseDictService.updateByIdSelective(baseDict));
    }

    @PostMapping("/deleteByIds")
    @ApiOperation(value = "批量删除", httpMethod = "POST")
    public Result<Boolean> deleteByIds(@RequestBody DeleteDto dto) {
        return Result.success(baseDictService.deleteByIds(dto.getIdList()));
    }

}
