package com.mitou.base.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mitou.base.entity.BaseRole;
import com.mitou.base.entity.dto.BaseUserRoleDto;
import com.mitou.base.entity.query.BaseRoleQuery;

import java.util.List;

/**
 * <p>
 * 系统角色 服务接口类
 * </p>
 *
 * @author rice
 * @since 2021-03-25
 */
public interface IBaseRoleService extends IService<BaseRole> {

    /**
     * 分页查询方法
     *
     * @param query
     * @return
     */
    IPage<BaseRole> page(BaseRoleQuery query);

    /**
     * 查询此用户下的角色集合
     *
     * @param userId
     * @return
     */
    List<BaseRole> getByUserId(Long userId);

    /**
     * 删除数据，并清除掉与此角色的菜单关联和此角色的用户关联
     *
     * @param roleIds
     * @return
     */
    boolean deleteByIds(List<Long> roleIds);

    /**
     * 为用户设置角色，会覆盖之前的角色
     *
     * @param baseUserRoleDto 关系
     * @return
     */
    boolean rel(BaseUserRoleDto baseUserRoleDto);

    /**
     * 获取默认角色
     *
     * @return
     */
    BaseRole getDefaultRole();
}
