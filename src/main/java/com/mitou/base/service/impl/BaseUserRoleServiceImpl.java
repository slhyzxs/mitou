package com.mitou.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mitou.base.entity.BaseUserRole;
import com.mitou.base.mapper.BaseUserRoleMapper;
import com.mitou.base.service.IBaseUserRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 用户角色关系 服务实现类
 * </p>
 *
 * @author rice
 * @since 2021-03-25
 */
@Service
public class BaseUserRoleServiceImpl extends ServiceImpl<BaseUserRoleMapper, BaseUserRole> implements IBaseUserRoleService {

    @Resource
    private BaseUserRoleMapper baseUserRoleMapper;

}
