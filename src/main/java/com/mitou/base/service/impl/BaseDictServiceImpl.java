package com.mitou.base.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mitou.base.entity.BaseDict;
import com.mitou.base.entity.query.BaseDictQuery;
import com.mitou.base.mapper.BaseDictMapper;
import com.mitou.base.service.IBaseDictService;
import com.mitou.common.utils.web.BaseDictUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 系统管理--数据字典 服务实现类
 * </p>
 *
 * @author rice
 * @since 2022-01-04
 */
@Service
public class BaseDictServiceImpl extends ServiceImpl<BaseDictMapper, BaseDict> implements IBaseDictService {

    @Resource
    private BaseDictMapper baseDictMapper;

    @Override
    public IPage<BaseDict> page(BaseDictQuery query) {
        LambdaQueryWrapper<BaseDict> lqw = new LambdaQueryWrapper<>();
        //进行条件组装
        //父id条件
        Long parentId = query.getParentId();
        lqw.eq(null != parentId, BaseDict::getParentId, parentId);
        return super.page(query.page(), lqw);
    }

    @Override
    public boolean insert(BaseDict baseDict) {
        boolean bool = super.save(baseDict);
        //操作成功后，重新初始化一下字典数据
        if (bool) {
            BaseDictUtil.initDictMap();
        }
        return bool;
    }

    @Override
    public boolean updateByIdSelective(BaseDict baseDict) {
        boolean bool = super.updateById(baseDict);
        //操作成功后，重新初始化一下字典数据
        if (bool) {
            BaseDictUtil.initDictMap();
        }
        return bool;
    }

    @Override
    public boolean deleteByIds(List<Long> ids) {
        boolean bool = super.removeByIds(ids);
        //操作成功后，重新初始化一下字典数据
        if (bool) {
            BaseDictUtil.initDictMap();
        }
        return bool;
    }

}
