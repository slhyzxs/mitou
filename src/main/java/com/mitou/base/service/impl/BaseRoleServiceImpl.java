package com.mitou.base.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mitou.base.entity.BaseRole;
import com.mitou.base.entity.BaseRoleMenu;
import com.mitou.base.entity.BaseUserRole;
import com.mitou.base.entity.dto.BaseUserRoleDto;
import com.mitou.base.entity.query.BaseRoleQuery;
import com.mitou.base.mapper.BaseRoleMapper;
import com.mitou.base.service.IBaseRoleMenuService;
import com.mitou.base.service.IBaseRoleService;
import com.mitou.base.service.IBaseUserRoleService;
import com.mitou.common.constants.BaseConstants;
import com.mitou.common.utils.web.BaseContextUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 系统角色 服务实现类
 * </p>
 *
 * @author rice
 * @since 2021-03-25
 */
@Service
public class BaseRoleServiceImpl extends ServiceImpl<BaseRoleMapper, BaseRole> implements IBaseRoleService {

    @Resource
    private BaseRoleMapper baseRoleMapper;
    @Resource
    private IBaseUserRoleService baseUserRoleService;
    @Resource
    private BaseContextUtil baseContextUtil;
    @Resource
    private IBaseRoleMenuService baseRoleMenuService;


    @Override
    public IPage<BaseRole> page(BaseRoleQuery query) {
        LambdaQueryWrapper<BaseRole> lqw = new LambdaQueryWrapper<>();
        //进行条件组装
        if (StringUtils.isNotBlank(query.getRoleName())) {
            lqw.like(BaseRole::getRoleName, query.getRoleName());
        }
        if (null != query.getSysInit()) {
            lqw.eq(BaseRole::getSysInit, query.getSysInit());
        }
        return super.page(query.page(), lqw);
    }

    @Override
    public List<BaseRole> getByUserId(Long userId) {
        if (null == userId) {
            userId = baseContextUtil.getUserId();
        }
        List<BaseRole> roleList = baseRoleMapper.getByUserId(userId);
        if (null == roleList) {
            roleList = new ArrayList<>();
        }
        return roleList;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteByIds(List<Long> roleIds) {
        //清除掉与此角色的菜单关联
        baseRoleMenuService.lambdaUpdate().in(BaseRoleMenu::getRoleId, roleIds).remove();
        //清除掉与此角色的用户关联
        baseUserRoleService.lambdaUpdate().in(BaseUserRole::getRoleId, roleIds).remove();
        return super.removeByIds(roleIds);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean rel(BaseUserRoleDto baseUserRoleDto) {
        Long userId = baseUserRoleDto.getUserId();
        List<Long> roleIdList = baseUserRoleDto.getRoleIdList();
        List<BaseUserRole> relList = new ArrayList<>();
        for (Long roleId : roleIdList) {
            BaseUserRole baseUserRole = new BaseUserRole();
            baseUserRole.setUserId(userId);
            baseUserRole.setRoleId(roleId);
            relList.add(baseUserRole);
        }
        //清除掉之前的角色
        baseUserRoleService.lambdaUpdate().in(BaseUserRole::getUserId, userId).remove();
        return baseUserRoleService.saveBatch(relList);
    }

    @Override
    public BaseRole getDefaultRole() {
        return super.getOne(
                new LambdaQueryWrapper<BaseRole>()
                        .eq(BaseRole::getRoleDefault, BaseConstants.DEFAULT_ROLE_TRUE)
                , false
        );
    }
}
