package com.mitou.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mitou.base.entity.BaseRoleMenu;
import com.mitou.base.mapper.BaseRoleMenuMapper;
import com.mitou.base.service.IBaseRoleMenuService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 角色权限关系 服务实现类
 * </p>
 *
 * @author rice
 * @since 2021-03-25
 */
@Service
public class BaseRoleMenuServiceImpl extends ServiceImpl<BaseRoleMenuMapper, BaseRoleMenu> implements IBaseRoleMenuService {

    @Resource
    private BaseRoleMenuMapper baseRoleMenuMapper;

}
