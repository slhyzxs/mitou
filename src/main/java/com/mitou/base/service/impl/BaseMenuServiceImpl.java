package com.mitou.base.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mitou.base.entity.BaseMenu;
import com.mitou.base.entity.BaseRoleMenu;
import com.mitou.base.entity.dto.BaseRoleMenuDto;
import com.mitou.base.entity.query.BaseMenuHasQuery;
import com.mitou.base.entity.query.BaseMenuQuery;
import com.mitou.base.entity.vo.BaseMenuTreeVo;
import com.mitou.base.entity.vo.BaseMenuVo;
import com.mitou.base.mapper.BaseMenuMapper;
import com.mitou.base.service.IBaseMenuService;
import com.mitou.base.service.IBaseRoleMenuService;
import com.mitou.common.utils.TreeUtil;
import com.mitou.common.utils.web.BaseContextUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 菜单 服务实现类
 * </p>
 *
 * @author rice
 * @since 2021-03-25
 */
@Service
public class BaseMenuServiceImpl extends ServiceImpl<BaseMenuMapper, BaseMenu> implements IBaseMenuService {

    @Resource
    private BaseMenuMapper baseMenuMapper;
    @Resource
    private BaseContextUtil baseContextUtil;
    @Resource
    private IBaseRoleMenuService baseRoleMenuService;

    @Override
    public IPage<BaseMenu> page(BaseMenuQuery baseMenuQuery) {
        LambdaQueryWrapper<BaseMenu> lqw = new LambdaQueryWrapper<>();
        //菜单名称条件
        if (StringUtils.isNotBlank(baseMenuQuery.getMenuName())) {
            lqw.like(BaseMenu::getMenuName, baseMenuQuery.getMenuName());
        }
        //菜单类型条件
        if (null != baseMenuQuery.getMenuType()) {
            lqw.eq(BaseMenu::getMenuType, baseMenuQuery.getMenuType());
        }
        return super.page(baseMenuQuery.page(), lqw);
    }

    @Override
    public List<BaseMenuTreeVo> getTree() {
        List<BaseMenuTreeVo> tree = new ArrayList<>();
        try {
            List<BaseMenuTreeVo> listVo = JSONObject.parseArray(JSON.toJSONString(super.list()), BaseMenuTreeVo.class);
            tree = TreeUtil.getTree(listVo, "menuId", "parentMenuId",
                    0L, "childrenList");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tree;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteByIds(List<Long> menuIds) {
        //清除掉与此菜单的角色关联
        baseRoleMenuService.lambdaUpdate().in(BaseRoleMenu::getMenuId, menuIds).remove();
        return super.removeByIds(menuIds);
    }

    @Override
    public List<BaseMenuVo> selectHas(BaseMenuHasQuery baseMenuHasQuery) {
        //默认查询当前用户
        if (null == baseMenuHasQuery.getUserId()) {
            baseMenuHasQuery.setUserId(baseContextUtil.getUserId());
        }
        List<BaseMenuVo> baseMenuVoList = baseMenuMapper.selectHas(
                baseMenuHasQuery.getUserId(),
                baseMenuHasQuery.getMenuType(),
                baseMenuHasQuery.getParentMenuId()
        );
        if (null == baseMenuVoList) {
            baseMenuVoList = new ArrayList<>();
        }
        return baseMenuVoList;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean rel(BaseRoleMenuDto baseRoleMenuDto) {
        Long roleId = baseRoleMenuDto.getRoleId();
        List<Long> menuIdList = baseRoleMenuDto.getMenuIdList();
        List<BaseRoleMenu> relList = new ArrayList<>();
        for (Long menuId : menuIdList) {
            BaseRoleMenu baseRoleMenu = new BaseRoleMenu();
            baseRoleMenu.setRoleId(roleId);
            baseRoleMenu.setMenuId(menuId);
            relList.add(baseRoleMenu);
        }
        //清除掉之前的菜单
        baseRoleMenuService.lambdaUpdate().eq(BaseRoleMenu::getRoleId, roleId).remove();
        return baseRoleMenuService.saveBatch(relList);
    }
}
