package com.mitou.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mitou.base.entity.BaseUserRole;

/**
 * <p>
 * 用户角色关系 服务接口类
 * </p>
 *
 * @author rice
 * @since 2021-03-25
 */
public interface IBaseUserRoleService extends IService<BaseUserRole> {

}
