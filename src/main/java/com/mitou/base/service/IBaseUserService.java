package com.mitou.base.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mitou.base.entity.BaseUser;
import com.mitou.base.entity.dto.BaseUserLoginDto;
import com.mitou.base.entity.dto.BaseUserSaveDto;
import com.mitou.base.entity.dto.BaseUserUpdatePwdDto;
import com.mitou.base.entity.query.BaseUserQuery;
import com.mitou.base.entity.vo.BaseUserLoginVo;
import com.mitou.base.entity.vo.BaseUserVo;
import com.mitou.common.response.Result;

import java.util.List;

/**
 * <p>
 * 系统用户 服务接口类
 * </p>
 *
 * @author rice
 * @since 2021-03-25
 */
public interface IBaseUserService extends IService<BaseUser> {

    /**
     * 查询用户信息
     *
     * @param userId
     * @return
     */
    BaseUserVo getById(Long userId);

    /**
     * 分页查询方法
     *
     * @param query
     * @return
     */
    IPage<BaseUser> page(BaseUserQuery query);

    /**
     * 新增，注册用户
     *
     * @param saveDto
     * @return
     */
    boolean register(BaseUserSaveDto saveDto);

    /**
     * 检查登录名的合法性，是否重复
     *
     * @param loginName
     * @return true：合法；false：不合法
     */
    boolean checkLoginNameLegal(String loginName);

    /**
     * 删除数据，并清除掉与此用户的角色关联
     *
     * @param userIds
     * @return
     */
    boolean deleteByIds(List<Long> userIds);

    /**
     * 用户登录
     *
     * @param baseUserLoginDto
     * @return
     */
    Result<BaseUserLoginVo> login(BaseUserLoginDto baseUserLoginDto);

    /**
     * 用户退出登录
     *
     * @return
     */
    boolean logout();

    /**
     * 更新用户密码
     *
     * @param updatePwdDto
     * @return
     */
    Result<Boolean> updatePwd(BaseUserUpdatePwdDto updatePwdDto);

}
