package com.mitou.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mitou.base.entity.BaseRoleMenu;

/**
 * <p>
 * 角色权限关系 服务接口类
 * </p>
 *
 * @author rice
 * @since 2021-03-25
 */
public interface IBaseRoleMenuService extends IService<BaseRoleMenu> {

}
