package com.mitou.base.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mitou.base.entity.BaseDict;
import com.mitou.base.entity.query.BaseDictQuery;

import java.util.List;

/**
 * <p>
 * 系统管理--数据字典 服务接口类
 * </p>
 *
 * @author rice
 * @since 2022-01-04
 */
public interface IBaseDictService extends IService<BaseDict> {

    /**
     * 分页查询方法
     *
     * @param query
     * @return
     */
    IPage<BaseDict> page(BaseDictQuery query);

    /**
     * 新增数据
     * 并更新字典缓存数据
     *
     * @param baseDict
     * @return
     */
    boolean insert(BaseDict baseDict);

    /**
     * 更新数据
     * 并更新字典缓存数据
     *
     * @param baseDict
     * @return
     */
    boolean updateByIdSelective(BaseDict baseDict);

    /**
     * 删除数据
     * 并更新字典缓存数据
     *
     * @param ids
     * @return
     */
    boolean deleteByIds(List<Long> ids);
}
