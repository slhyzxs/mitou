package com.mitou.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mitou.base.entity.BaseDict;

/**
 * <p>
 * 数据字典 Mapper 接口
 * </p>
 *
 * @author rice
 * @since 2022-01-04
 */
public interface BaseDictMapper extends BaseMapper<BaseDict> {

}
