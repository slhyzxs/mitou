package com.mitou.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mitou.base.entity.BaseUser;

/**
 * <p>
 * 系统用户 Mapper 接口
 * </p>
 *
 * @author rice
 * @since 2021-03-25
 */
public interface BaseUserMapper extends BaseMapper<BaseUser> {

}
