package com.mitou.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mitou.base.entity.BaseUserRole;

/**
 * <p>
 * 用户角色关系 Mapper 接口
 * </p>
 *
 * @author rice
 * @since 2021-03-25
 */
public interface BaseUserRoleMapper extends BaseMapper<BaseUserRole> {

}
