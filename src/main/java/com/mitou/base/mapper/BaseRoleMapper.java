package com.mitou.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mitou.base.entity.BaseRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 系统角色 Mapper 接口
 * </p>
 *
 * @author rice
 * @since 2021-03-25
 */
public interface BaseRoleMapper extends BaseMapper<BaseRole> {

    /**
     * 查询用户的角色列表
     *
     * @param userId 用户id
     * @return 角色列表
     */
    public List<BaseRole> getByUserId(@Param("userId") Long userId);

}
