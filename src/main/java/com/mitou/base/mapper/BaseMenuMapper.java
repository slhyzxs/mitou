package com.mitou.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mitou.base.entity.BaseMenu;
import com.mitou.base.entity.vo.BaseMenuVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 菜单 Mapper 接口
 * </p>
 *
 * @author rice
 * @since 2021-03-25
 */
public interface BaseMenuMapper extends BaseMapper<BaseMenu> {

    /**
     * 查询当前用户所拥有的菜单列表
     *
     * @param userId       用户id
     * @param menuType     菜单类型
     * @param parentMenuId 菜单父id
     * @return 列表集合
     */
    public List<BaseMenuVo> selectHas(@Param("userId") Long userId, @Param("menuType") Integer menuType, @Param("parentMenuId") Long parentMenuId);
}
