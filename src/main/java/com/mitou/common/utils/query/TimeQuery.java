package com.mitou.common.utils.query;

import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 分页查询的时间、分页参数
 * </p>
 *
 * @author rice
 * @since 2022-01-07
 */
@Getter
@Setter
public class TimeQuery extends PageQuery {

    private static final long serialVersionUID = 1L;

    @ApiParam(name = "startDate", value = "查询开始时间")
    private String startDate;

    @ApiParam(name = "endDate", value = "查询结束时间")
    private String endDate;

}
