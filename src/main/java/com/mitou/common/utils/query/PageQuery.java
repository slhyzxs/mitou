package com.mitou.common.utils.query;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Positive;

/**
 * <p>
 * 分页查询的分页参数
 * </p>
 *
 * @author rice
 * @since 2021-10-15
 */
@Getter
@Setter
public class PageQuery {

    @ApiParam(name = "pageNo", value = "当前页")
    @Positive
    private Long pageNo;

    @ApiParam(name = "pageSize", value = "每页记录数")
    @Positive
    private Long pageSize;

    public <T> IPage<T> page() {
        if (null == pageNo || null == pageSize) {
            return new Page<>();
        }
        return new Page<>(pageNo, pageSize);
    }
}
