package com.mitou.common.utils.web;

import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * redis工具类
 *
 * @author rice
 * @since 2021-03-25
 */
@Component
public class RedisCacheUtil {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 设置hash key、字段、值
     *
     * @param key   redis key
     * @param field hash表字段
     * @param value hash表字段值
     */
    public void hashSet(String key, String field, String value) {
        if (key == null || "".equals(key)) {
            return;
        }
        stringRedisTemplate.opsForHash().put(key, field, value);
    }

    /**
     * 获取hash表、字段、值
     *
     * @param key   redis key
     * @param field hash表字段
     */
    public String hashGet(String key, String field) {
        if (key == null || "".equals(key)) {
            return null;
        }
        return (String) stringRedisTemplate.opsForHash().get(key, field);
    }

    /**
     * 检查字段是否存在
     *
     * @param key   redis key
     * @param field hash表字段
     */
    public boolean hashExists(String key, String field) {
        if (key == null || "".equals(key)) {
            return false;
        }
        return stringRedisTemplate.opsForHash().hasKey(key, field);
    }

    /**
     * 获取hash元素个数
     *
     * @param key redis key
     */
    public long hashSize(String key) {
        if (key == null || "".equals(key)) {
            return 0L;
        }
        return stringRedisTemplate.opsForHash().size(key);
    }

    /**
     * 删除hash内某字段
     *
     * @param key   redis key
     * @param field hash表字段
     */
    public void hashDel(String key, String field) {
        if (key == null || "".equals(key)) {
            return;
        }
        stringRedisTemplate.opsForHash().delete(key, field);
    }

    /**
     * 存放普通键值对
     *
     * @param key   redis key
     * @param value redis value
     */
    public void setStr(String key, String value) {
        stringRedisTemplate.opsForValue().set(key, value);
    }

    /**
     * 存放普通键值对并设置过期时间
     *
     * @param key      redis key
     * @param value    redis value
     * @param l        时长
     * @param timeUnit 单位 {@link TimeUnit}
     */
    public void setStr(String key, String value, int l, TimeUnit timeUnit) {
        stringRedisTemplate.opsForValue().set(key, value, l, timeUnit);
    }

    /**
     * 设置过期时间
     *
     * @param key      redis key
     * @param l        时长
     * @param timeUnit 单位 {@link TimeUnit}
     */
    public void expire(String key, int l, TimeUnit timeUnit) {
        stringRedisTemplate.expire(key, l, timeUnit);
    }

    /**
     * 取普通键值对
     *
     * @param key redis key
     */
    public String getStr(String key) {
        return stringRedisTemplate.opsForValue().get(key);
    }

    /**
     * 检查字段是否存在
     *
     * @param key redis key
     */
    public Boolean hasKey(String key) {
        return stringRedisTemplate.hasKey(key);
    }

    /**
     * 删除key
     *
     * @param key redis key
     */
    public Boolean deleteStr(String key) {
        return stringRedisTemplate.delete(key);
    }

}
