package com.mitou.common.utils;

import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.core.toolkit.LambdaUtils;
import com.baomidou.mybatisplus.core.toolkit.support.LambdaMeta;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;

/**
 * mybatis-plus相关工具使用
 *
 * @author rice
 * @since 2021-09-13
 */
public class MpUtil {

    /**
     * 获取表内字段
     *
     * @param function 字段
     * @return java.lang.String 数据库表的字段名
     */
    public static <R, T> String getColumn(SFunction<T, R> function) {
        try {
            LambdaMeta extract = LambdaUtils.extract(function);
            return LambdaUtils.getColumnMap(extract.getInstantiatedClass())
                    .get(LambdaUtils.formatKey(extract.getImplMethodName().substring(3)))
                    .getColumn();
        } catch (Exception e) {
            throw new MybatisPlusException("未找到该字段");
        }
    }

}
