package com.mitou.common.utils;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * bean转换工具类
 *
 * @author rice
 * @since 2022-01-06
 */
public class BeanConvertUtils {

    /**
     * 同：BeanUtils.copyProperties(dtoEntity, newInstance);
     *
     * @param obj   原数据--Dto，Vo，entity
     * @param clazz 要转换的类
     * @return 转换后的对象
     */
    public static <E> E convert(Object obj, Class<E> clazz) {
        if (obj == null || clazz == null) {
            return null;
        }
        try {
            // 创建新的对象实例
            E newInstance = clazz.getDeclaredConstructor().newInstance();
            // 把原对象数据拷贝到新的对象
            org.springframework.beans.BeanUtils.copyProperties(obj, newInstance);
            // 返回新对象
            return newInstance;
        } catch (Exception e) {
            return null;
        }
    }


    /**
     * Page<Entity> 分页对象转 Page<Vo>  ( list 循环)
     *
     * @param page  mp分页对象
     * @param clazz 要转换的类
     */
    public static <T, V> IPage<V> pageVo(IPage<T> page, Class<V> clazz) {
        return page.convert(item -> convert(item, clazz));
    }


    /**
     * list<Entity> 集合对象转list<Vo> (list循环)
     *
     * @param oldList list数据
     * @param clazz   要转换的类
     */
    public static <T, V> List<V> listVo(List<T> oldList, Class<V> clazz) {
        if (CollectionUtils.isEmpty(oldList)) {
            return null;
        }
        List<V> voList = new ArrayList<>();
        oldList.forEach(i -> voList.add(convert(i, clazz)));
        return voList;
    }


    /**
     * list<Entity> 集合对象转list<Vo> (Stream方式)
     *
     * @param oldList list数据
     * @param clazz   要转换的类
     */
    public static <T, V> List<V> listVoStream(List<T> oldList, Class<V> clazz) {
        if (CollectionUtils.isEmpty(oldList)) {
            return null;
        }
        return oldList.stream().map(item -> convert(item, clazz)).collect(Collectors.toList());
    }
}

