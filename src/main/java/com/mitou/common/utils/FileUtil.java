package com.mitou.common.utils;

import java.io.File;

/**
 * 文件操作工具类
 *
 * @author rice
 * @since 2021-11-23
 */
public class FileUtil {

    /**
     * 删除目录/文件
     *
     * @param dir 目录/文件
     * @return
     */
    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            if (null != children && children.length > 0) {
                for (String child : children) {
                    boolean success = deleteDir(new File(dir, child));
                    if (!success) {
                        return false;
                    }
                }
            }
        }
        return dir.delete();
    }
}
