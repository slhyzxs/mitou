package com.mitou.common.utils.dto;

import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * <p>
 * 删除请求入参
 * </p>
 *
 * @author rice
 * @since 2021-10-15
 */
@Getter
@Setter
public class DeleteDto {

    @ApiParam(name = "pageNo", value = "要删除的id数组")
    private List<Long> idList;

}
