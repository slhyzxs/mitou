package com.mitou.common.constants;

/**
 * <p>
 * 系统常量类
 * <p/>
 *
 * @author rice
 * @since 2021-03-25
 */
public class BaseConstants {

    /**
     * 与前端约定的存放于header中的token名
     */
    public static final String TOKEN_MAME = "Authorization";

    /**
     * 系统token有效时长(小时)
     */
    public static final int TOKEN_DURATION_TIME = 2;

    /**
     * 系统中默认顶级父id
     */
    public static final Long DEFAULT_PARENT_ID = 0L;

    /**
     * 系统中默认密码
     */
    public static final String DEFAULT_PWD = "mitou123";

    /**
     * 删除状态：正常/未删除
     */
    public static final Integer DEL_FALSE = 0;
    /**
     * 删除状态：已删除
     */
    public static final Integer DEL_TRUE = 1;

    /**
     * 启用状态：未启用
     * 发布状态：未发布
     */
    public static final Integer ENABLE_FALSE = 0;
    /**
     * 启用状态：已启用
     * 发布状态：已发布
     */
    public static final Integer ENABLE_TRUE = 1;

    /**
     * 审批状态：待审批、审批中
     */
    public static final Integer APPROVE_ING = 0;
    /**
     * 审批状态：审批通过
     */
    public static final Integer APPROVE_SUCCESS = 1;
    /**
     * 审批状态：审批未通过
     */
    public static final Integer APPROVE_FAIL = 2;

    /*以下为系统中业务常量***********************************************************/

    /**
     * 是否为默认角色：是
     */
    public static final Integer DEFAULT_ROLE_TRUE = 1;
}
