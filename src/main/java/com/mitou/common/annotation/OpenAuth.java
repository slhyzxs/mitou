package com.mitou.common.annotation;

import java.lang.annotation.*;

/**
 * <p>
 * 开放权限
 * <p/>
 *
 * @author rice
 * @since 2021-10-15
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OpenAuth {
}
