package com.mitou.common.annotation;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mitou.common.interceptor.JsonBindSerializer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * 字典值绑定
 * </p>
 *
 * @author rice
 * @since 2022-01-16
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@JacksonAnnotationsInside
@JsonSerialize(using = JsonBindSerializer.class)
public @interface JsonBindForDict {
}
