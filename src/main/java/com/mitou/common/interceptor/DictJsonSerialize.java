package com.mitou.common.interceptor;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.mitou.common.utils.web.BaseDictUtil;

import java.io.IOException;

/**
 * 字段值转换
 *
 * @author rice
 * @since 2022-01-12
 */
public class DictJsonSerialize extends JsonSerializer<Long> {

    @Override
    public void serialize(Long arg0, JsonGenerator arg1, SerializerProvider serializerProvider) throws IOException {
        arg1.writeString(BaseDictUtil.getNameById(arg0));
    }
}
